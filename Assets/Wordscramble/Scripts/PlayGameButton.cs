﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayGameButton : MonoBehaviour
{
    // Start is called before the first frame update
    public Button playButton;
    public Button hubButton;

    private void Start()
    {
        var playBtn = playButton.GetComponent<Button>();
        playBtn.onClick.AddListener(PlayGame);

        var hubBtn = hubButton.GetComponent<Button>();
        hubBtn.onClick.AddListener(BackToHub);
    }

    private static void BackToHub()
    {
        Destroy(GameObject.Find("SkinManager"));
        SceneManager.LoadScene("Scenes/Main");
    }

    private static void PlayGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
