﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class WordScramble : MonoBehaviour
{
    [SerializeField]
    private List<Word> _words = new List<Word>();

    [SerializeField]
    public List<State> states = new List<State>();

    [SerializeField]
    private List<string> disabled_letters = new List<string>();

    public List<Word> words
    {
        get { return _words; }
        set { _words = value; }
    }

    private int answer_count = 0;
    private string correct_answer;
    private string answer_input = "";
    private Word active_word;
    int index;
    int score = 0;

    [SerializeField]
    State state;

    public GameObject skin;

    // Start is called before the first frame update
    void Start()
    {
        skin = GameObject.Find("SkinManager");
        ReadWordsFromFile();
        var random = new System.Random();
        index = random.Next(words.Count);
        //Debug.Log(words[index].word);
        correct_answer = words[index].word;
        active_word = words[index];

        //SpawnGameObjects();

        states.Add(new State(active_word, answer_count, correct_answer, answer_input, index));

        state = states[states.Count - 1];

        InstaitateFromState();
    }

    void InstaitateFromState() {
        //State state = new State(active_word, answer_count, correct_answer, answer_input, index, new GameObject(), new GameObject());

        //State state = new State(new Word("foxtrot", "hint", 3, GetTilesFromWord("foxtrot")), 1, "foxtrot", "o", index);

        
        
        int counter = 1;

        foreach (Tile c in state.word.tiles) {
            //Answer Tiles
            GameObject answer = new GameObject("answer_" + counter);
            Image answer_image = answer.AddComponent<Image>();

            if (state.answer_count >= counter)
            {
                answer_image.sprite = Resources.Load<Sprite>("Wordscramble/Sprites/LetterTiles_"+skin.GetComponent<SkinManager>().ActiveSkin+"/letter_" + state.answer_input[counter-1].ToString().ToUpper());
            }
            else 
            {
                answer_image.sprite = Resources.Load<Sprite>("Wordscramble/Sprites/LetterTiles_" + skin.GetComponent<SkinManager>().ActiveSkin + "/letter");
            }

            Button answer_button = answer.AddComponent<Button>();
            answer_button.onClick.AddListener(delegate () { AnswerClick(answer, c, answer_image); });
            answer.transform.SetParent(GameObject.Find("AnswerField").transform, false);

            //Letter Tiles
            GameObject letter = new GameObject(c.letter.ToString() + "_" + counter);
            Image letter_image = letter.AddComponent<Image>();
            letter_image.sprite = Resources.Load<Sprite>("Wordscramble/Sprites/LetterTiles_" + skin.GetComponent<SkinManager>().ActiveSkin + "/letter_" + c.letter.ToString().ToUpper());
            Button letter_button = letter.AddComponent<Button>();
            letter_button.onClick.AddListener(delegate () { TileClick(letter, c.letter, letter_image); });
            letter.transform.SetParent(GameObject.Find("LetterField").transform, false);

            foreach (var item in disabled_letters)
            {
                if (c.letter.ToString() + "_" + counter == item)
                {
                    letter.gameObject.SetActive(false);
                }
            }

            counter++;
        }

        foreach (var item in GameObject.Find("LetterField").transform.GetComponentsInChildren<Transform>())
        {
            foreach (var c in state.answer_input)
            {
                if (item.name == c.ToString())
                {
                    //item.gameObject.SetActive(false);
                }
            }
            //Debug.Log(item.name);
        }

        //Debug.Log("START");
        foreach (var c in state.answer_input)
        {
            //Debug.Log(GameObject.Find("LetterField").transform.Find(c.ToString()).GetComponent<Image>().sprite.name);
            //GameObject.Find("LetterField").transform.Find(c.ToString()).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/LetterTiles/letter");
            //Debug.Log(GameObject.Find("LetterField").transform.Find(c.ToString()).GetComponent<Image>().sprite.name);
        }
        //Debug.Log("END");
    }

    // Update is called once per frame
    void Update()
    {
        if (state.answer_input.Length == state.correct_answer.Length) {
            if (state.answer_input == state.correct_answer && words.Count > 0)
            {
                Debug.Log("Correct Answer");
                score++;
                GameObject.Find("Score").GetComponent<TMP_Text>().text = "Score: " + score.ToString();
                Skip();

                /*if (words.Count == 1)
                {
                    words.RemoveAt(index);
                    score++;
                    GameObject.Find("Score").GetComponent<TMP_Text>().text = "Score: " + score.ToString();
                }*/
            }
            else if (words.Count == 0)
            {
                states.Clear();
                GameObject.Find("ScoreTracker").GetComponent<ScoreTracker>().score = score;
                SceneManager.LoadScene("WinScene");
                //GameObject.Find("Hint").GetComponent<TMP_Text>().text = "Game Done! \n You got " + score + " points";
            }
            else
            {
                Debug.Log("Wrong Answer");
            }
        }

        if (words.Count == 0)
        {
            GameObject.Find("ScoreTracker").GetComponent<ScoreTracker>().score = score;
            SceneManager.LoadScene("WinScene");
        }

        if (Input.deviceOrientation == DeviceOrientation.Portrait) {
            //GameObject.Find("Portrait").SetActive(true);
            //GameObject.Find("Landscape").SetActive(false);
            GameObject.Find("Canvas").GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
            GameObject.Find("AnswerField").GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
            GameObject.Find("LetterField").GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
        } else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight) {
            GameObject.Find("Canvas").GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
            GameObject.Find("AnswerField").GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
            GameObject.Find("LetterField").GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
            //GameObject.Find("Landscape").SetActive(true);
            //GameObject.Find("Portrait").SetActive(false);
        }
    }

    void TileClick(GameObject caller, char letter, Image letter_image) {
        //Debug.Log(letter_image);
        GameObject answer_field = GameObject.Find("AnswerField");
        answer_field.transform.GetChild(state.answer_count).GetComponent<Image>().sprite = letter_image.sprite;
        caller.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/Sprites/LetterTiles_" + skin.GetComponent<SkinManager>().ActiveSkin + "/letter");
        caller.transform.GetComponent<Button>().enabled = false;
        var tmp_answer_count = state.answer_count;
        var tmp_answer_input = state.answer_input;

        disabled_letters.Add(caller.name);
        caller.SetActive(false);

        State next_state = new State(state.word, tmp_answer_count+=1, state.correct_answer, tmp_answer_input += letter, state.index);

        states.Add(next_state);

        state = next_state;

    }

    void AnswerClick(GameObject caller, Tile tile, Image letter_image) {
        if (letter_image.sprite.name != "letter")
        {
            //Debug.Log("LETTER: " + tile.letter);
            //Debug.Log("POSITION: " + tile.position);
            //Debug.Log("LETTER FIELD: " + GameObject.Find("LetterField").transform.GetChild(tile.position).transform.gameObject.name);
            

            
            //letter_image.sprite = Resources.Load<Sprite>("Sprites/LetterTiles/letter");
            //GameObject.Find("LetterField").transform.GetChild(tile.position).transform.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/LetterTiles/letter_" + tile.letter.ToString().ToUpper());
        }
    }

    public void Undo() {

        if (states.Count > 1)
        {
            foreach (Transform child in GameObject.Find("AnswerField").transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            foreach (Transform child in GameObject.Find("LetterField").transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            states.RemoveAt(states.Count - 1);

            state = states[states.Count - 1];

            disabled_letters.RemoveAt(disabled_letters.Count - 1);

            //SpawnGameObjects();

            InstaitateFromState();
        }
    }

    public void Skip() {

        if (words.Count > 0)
        {
            GameObject.Find("Hint_text").GetComponent<TMP_Text>().enabled = false;
            words.RemoveAt(index);

            var random = new System.Random();
            index = random.Next(words.Count);

            foreach (Transform child in GameObject.Find("AnswerField").transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            foreach (Transform child in GameObject.Find("LetterField").transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            //GameObject.Find("Hint").GetComponent<TMP_Text>().text = "";

            //SpawnGameObjects();

            states.Clear();

            //Debug.Log(words[index].word);
            //Debug.Log(index);

            states.Add(new State(words[index], 0, words[index].word, "", index));

            state = states[states.Count - 1];

            disabled_letters.Clear();

            InstaitateFromState();
        }
    }

    public void GetHint() {
        GameObject.Find("Hint_text").GetComponent<TMP_Text>().enabled = true;
        Debug.Log(state.word.hint);
        //GameObject.Find("Hint").GetComponent<Image>().sprite = Resources.Load<Sprite>(state.word.hint);
        GameObject.Find("Hint_text").GetComponent<TMP_Text>().text = state.word.hint;
    }

    void ReadWordsFromFile() {
        string line;

        TextAsset doc = Resources.Load<TextAsset>("Wordscramble/Words");

        string fs = doc.text;
        string[] fLines = Regex.Split(fs, "\n|\r|\r\n");

        for (int i = 0; i < fLines.Length; i++)
        {
            if (fLines[i] != "")
            {
                string[] wordAndClue = fLines[i].Split('-');
                //Debug.Log("HERE START");
                //Debug.Log(wordAndClue[0].Trim());
                //Debug.Log(wordAndClue[0]);
                //Debug.Log(wordAndClue[1]);
                //Debug.Log(wordAndClue[0].Trim().Length);
                //Debug.Log("HERE END");
                words.Add(new Word(wordAndClue[0].Trim().ToLower(), wordAndClue[1], wordAndClue[0].Trim().Length, ShuffleList(GetTilesFromWord(wordAndClue[0].Trim().ToLower()))));
            }
        }

        // Read the file and display it line by line.  
        System.IO.StreamReader file =
            new System.IO.StreamReader("Assets/Resources/Wordscramble/Words.txt");
        while ((line = file.ReadLine()) != null)
        {
            //words.Add(new Word(line, "This is a hint", line.Length, ShuffleList(GetTilesFromWord(line))));
        }

        file.Close();
    }

    List<Tile> GetTilesFromWord(string word) {
        List<Tile> tiles = new List<Tile>();
        int counter = 0;
        foreach (char c in word)
        {
            tiles.Add(new Tile(c, counter));
            counter++;
        }
        return tiles;
    }

    IList<E> ShuffleList<E>(IList<E> list)
    {
        var random = new System.Random();

        if (list.Count > 1)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                E tmp = list[i];
                int randomIndex = random.Next(i + 1);

                //Swap elements
                list[i] = list[randomIndex];
                list[randomIndex] = tmp;
            }
        }

        return list;
    }
}
