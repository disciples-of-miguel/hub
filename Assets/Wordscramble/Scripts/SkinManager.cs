﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SkinManager : MonoBehaviour
{
    [System.Serializable]
    public enum Reskins { 
        Default,
        Broendby
    };

    public Reskins ActiveSkin = Reskins.Default;

    Scene current_scene;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        current_scene = SceneManager.GetActiveScene();
        DontDestroyOnLoad(this.gameObject);
        applySkin(ActiveSkin);
    }

    // Update is called once per frame
    void Update()
    {
        //Scene was changed
        if (SceneManager.GetActiveScene() != current_scene)
        {
            current_scene = SceneManager.GetActiveScene();
            applySkin(ActiveSkin);
        }
    }

    void applySkin(Reskins skin) {
        if (current_scene.name == "GameScene")
        {
            if (skin == Reskins.Broendby)
            {
                GameObject.Find("GetHint").GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/UI/HintColor_Broendby");
                GameObject.Find("Undo").GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/UI/ArrowLeft_Broendby");
                GameObject.Find("Skip").GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/UI/ArrowRight_Broendby");
            }
            else if (skin == Reskins.Default)
            {
                
            }
        }
        else if (current_scene.name == "WinScene")
        {
            if (skin == Reskins.Broendby)
            {
                GameObject.Find("Logo").GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/Sprites/logo");
            }
            else if (skin == Reskins.Default)
            {
                GameObject.Find("Logo").GetComponent<Image>().sprite = Resources.Load<Sprite>("Wordscramble/Sprites/BillettoLogo");
            }
        }
    }
}
