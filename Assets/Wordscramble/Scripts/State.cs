﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class State
{

    [SerializeField]
    private Word _word;
    [SerializeField]
    private int _answer_count;
    [SerializeField]
    private string _correct_answer;
    [SerializeField]
    private string _answer_input;
    [SerializeField]
    private int _index;

    public State(Word word, int answer_count, string correct_answer, string answer_input, int index)
    {
        this._word = word;
        this._answer_count = answer_count;
        this._correct_answer = correct_answer;
        this._answer_input = answer_input;
        this._index = index;
    }

    public Word word
    {
        get { return _word; }
        set { _word = value; }
    }

    public int answer_count
    {
        get { return _answer_count; }
        set { _answer_count = value; }
    }

    public string correct_answer
    {
        get { return _correct_answer; }
        set { _correct_answer = value; }
    }

    public string answer_input
    {
        get { return _answer_input; }
        set { _answer_input = value; }
    }

    public int index
    {
        get { return _index; }
        set { _index = value; }
    }
}
