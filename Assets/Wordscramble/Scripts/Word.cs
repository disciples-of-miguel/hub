﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Word
{
    [SerializeField]
    private string _word;
    [SerializeField]
    private string _hint;
    [SerializeField]
    private int _length;
    [SerializeField]
    private IList<Tile> _tiles;

    public Word(string word, string hint, int length, IList<Tile> tiles)
    {
        this._word = word;
        this._hint = hint;
        this._length = length;
        this._tiles = tiles;
    }

    public string word
    {
        get { return _word; }
        set { _word = value; }
    }

    public string hint
    {
        get { return _hint; }
        set { _hint = value; }
    }

    public int length
    {
        get { return _length; }
        set { _length = value; }
    }

    public IList<Tile> tiles
    {
        get { return _tiles; }
        set { _tiles = value; }
    }
}
