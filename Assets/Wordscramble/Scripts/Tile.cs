﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Tile
{
    [SerializeField]
    private char _letter;
    [SerializeField]
    private int _position;

    public char letter
    {
        get { return _letter; }
        set { _letter = value; }
    }

    public int position
    {
        get { return _position; }
        set { _position = value; }
    }

    public Tile(char letter)
    {
        this._letter = letter;
        this._position = 0;
    }

    public Tile(char letter, int position)
    {
        this._letter = letter;
        this._position = position;
    }
}
