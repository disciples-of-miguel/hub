﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Net;
using System.Security.Claims;

public class menuSystem : MonoBehaviour
{

    public GameObject GameView;
    public GameObject GameInfo;

    private List<EventModel> events;
    private List<EventModel> eventsFromOrg;
    private List<LeaderboardModel> leaderbord;
    private List<(string, int)> best_players;

    public GameObject skinManager;

    private void Start()
    {
        
    }

    // Start is called before the first frame update
    void Awake()
    {
        events = GetActiveGames();
        leaderbord = GetLeaderboardFromEvent(1);

        best_players = new List<(string, int)>();

        for (int i = 0; i < leaderbord[0].Scores.Count; i++)
        {
            best_players.Add(new ValueTuple<string, int>(leaderbord[0].Players[i], leaderbord[0].Scores[i]));
        }

        best_players.Sort((x, y) => {
            int result = y.Item2.CompareTo(x.Item2);
            return result == 0 ? y.Item2.CompareTo(x.Item2) : result;
        });

        for (int i = events.Count - 1; i >= 0; i--)
        {
            eventsFromOrg = GetEventsFromOrg(events[i].OrganizerID);
            eventsFromOrg = eventsFromOrg.Where(x => x.EventID != events[i].EventID).ToList();
            InstantiateGameView(events[i], eventsFromOrg);
        }

        foreach (var item in events)
        {
            //InstantiateGameView(item as EventModel);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DisplayInfoView(string name) {
        GameObject info_view = transform.Find("GameInfoView").Find(name).transform.gameObject;
        info_view.transform.gameObject.GetComponent<Animator>().Play("info_view_slide_in");
    }

    public void HideInfoView(string name) {
        GameObject info_view = transform.Find("GameInfoView").Find(name).transform.gameObject;
        info_view.transform.gameObject.GetComponent<Animator>().Play("info_view_slide_out");
    }

    List<EventModel> GetActiveGames() {


        string urlParameters = "?_select=*";
        string responseBody = "";

        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri("http://thesis.espnielsen.dk:3000/Games/public/Events");
        client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

        //TODO - Generate new JWT token as needed
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbCI6ImFkbWluaXN0cmF0b3JAdGhlc2lzLmVzcG5pZWxzZW4uZGsiLCJuYmYiOjE1ODkyNzU2NzIsImV4cCI6MTU4OTg4MDQ3MiwiaWF0IjoxNTg5Mjc1NjcyfQ.sMG_rv2ZRqum6glcrjFWJKsaMaz255oJTUMpn1swkkY");

        HttpResponseMessage response = client.GetAsync(urlParameters).Result;

        if (response.IsSuccessStatusCode)
        {
            responseBody = response.Content.ReadAsStringAsync().Result;
            Debug.Log("SUCCESS");
            Debug.Log((int)response.StatusCode);
            Debug.Log(responseBody);
        }
        else
        {
            Debug.Log("ERROR");
            Debug.Log((int)response.StatusCode);
            Debug.Log(response.ReasonPhrase.ToString());
        }

        client.Dispose();

        //Debug.Log(responseBody);

        List<EventModel> json = JsonConvert.DeserializeObject<List<EventModel>>(responseBody);

        //Debug.Log(json[0].Name);

        return json;
    }

    List<EventModel> GetEventsFromOrg(int id) {
        Debug.Log("ID: " + id);
        string urlParameters = "?OrganizerID=$eq." + id;
        string responseBody = "";

        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri("http://thesis.espnielsen.dk:3000/Games/public/Events");
        client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

        //TODO - Generate new JWT token as needed
        client.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbCI6ImFkbWluaXN0cmF0b3JAdGhlc2lzLmVzcG5pZWxzZW4uZGsiLCJuYmYiOjE1ODkyNzU2NzIsImV4cCI6MTU4OTg4MDQ3MiwiaWF0IjoxNTg5Mjc1NjcyfQ.sMG_rv2ZRqum6glcrjFWJKsaMaz255oJTUMpn1swkkY");

        HttpResponseMessage response = client.GetAsync(urlParameters).Result;

        if (response.IsSuccessStatusCode)
        {
            responseBody = response.Content.ReadAsStringAsync().Result;
            Debug.Log("SUCCESS");
            Debug.Log((int)response.StatusCode);
            //Debug.Log(responseBody);
        }
        else
        {
            Debug.Log("ERROR");
            Debug.Log((int)response.StatusCode);
            //Debug.Log(response.ReasonPhrase.ToString());
        }

        client.Dispose();

        //Debug.Log(responseBody);

        List<EventModel> json = JsonConvert.DeserializeObject<List<EventModel>>(responseBody);

        return json;
    }

    List<LeaderboardModel> GetLeaderboardFromEvent(int id)
    {
        Debug.Log("ID: " + id);
        string urlParameters = "?ID=$eq." + id;
        string responseBody = "";

        HttpClient client = new HttpClient();
        client.BaseAddress = new Uri("http://thesis.espnielsen.dk:3000/Games/public/Leaderboards");
        client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

        //TODO - Generate new JWT token as needed
        client.DefaultRequestHeaders.Authorization =
            new AuthenticationHeaderValue("Bearer", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluaXN0cmF0b3IiLCJlbWFpbCI6ImFkbWluaXN0cmF0b3JAdGhlc2lzLmVzcG5pZWxzZW4uZGsiLCJuYmYiOjE1ODkyNzU2NzIsImV4cCI6MTU4OTg4MDQ3MiwiaWF0IjoxNTg5Mjc1NjcyfQ.sMG_rv2ZRqum6glcrjFWJKsaMaz255oJTUMpn1swkkY");

        HttpResponseMessage response = client.GetAsync(urlParameters).Result;

        if (response.IsSuccessStatusCode)
        {
            responseBody = response.Content.ReadAsStringAsync().Result;
            Debug.Log("SUCCESS");
            Debug.Log((int)response.StatusCode);
            //Debug.Log(responseBody);
        }
        else
        {
            Debug.Log("ERROR");
            Debug.Log((int)response.StatusCode);
            //Debug.Log(response.ReasonPhrase.ToString());
        }

        client.Dispose();

        List<LeaderboardModel> json = JsonConvert.DeserializeObject<List<LeaderboardModel>>(responseBody);

        return json;
    }

    void InstantiateGameView(EventModel e, List<EventModel> eventsFromOrg) {
        Texture2D text = new Texture2D(1, 1);
        text.LoadImage(Convert.FromBase64String(e.Image));

        Sprite sprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(1f, .1f));
        //image.sprite = sprite; //Image is a defined reference to an image component


        GameObject gv = Instantiate(GameView);
        gv.name = "gv_" + e.EventID.ToString();
        gv.GetComponent<Image>().sprite = sprite;
        gv.GetComponent<Button>().onClick.AddListener(() => DisplayInfoView("gv_" + e.EventID.ToString()));
        gv.transform.SetParent(GameObject.Find("GamesView").transform, false);

        GameObject gi = Instantiate(GameInfo);
        gi.name = "gv_" + e.EventID.ToString();
        gi.transform.Find("Header_text").GetComponent<TMP_Text>().text = e.Name;
        gi.transform.Find("Description_text").GetComponent<TMP_Text>().text = e.Description;
        gi.transform.Find("Leaderboard_text").GetComponent<TMP_Text>().text = "<b>Leaderboard!</b>\n"
            + best_players[0].Item1 + " - " + best_players[0].Item2 + "\n"
            + best_players[1].Item1 + " - " + best_players[1].Item2 + "\n"
            + best_players[2].Item1 + " - " + best_players[2].Item2 + "\n"
            + best_players[3].Item1 + " - " + best_players[3].Item2 + "\n"
            + best_players[4].Item1 + " - " + best_players[4].Item2;
        gi.transform.Find("Back_btn").GetComponent<Button>().onClick.AddListener(() => HideInfoView("gv_" + e.EventID.ToString()));
        gi.transform.SetParent(GameObject.Find("GameInfoView").transform, false);
        foreach (var item in eventsFromOrg)
        {
            GameObject go = new GameObject();
            Image img = go.AddComponent<Image>();
            Texture2D text2 = new Texture2D(1, 1);
            text2.LoadImage(Convert.FromBase64String(item.Image));
            Sprite sprite2 = Sprite.Create(text2, new Rect(0, 0, text2.width, text2.height), new Vector2(1f, .1f));
            img.sprite = sprite2;
            go.GetComponent<RectTransform>().sizeDelta = new Vector2(750, 400);
            go.transform.SetParent(gi.transform.Find("OtherEvents").Find("OtherEvents_Images").transform, false);
            Debug.Log("OTHER EVENT: " + item.Name);
        }

        gi.AddComponent<GameInformation>();
        gi.GetComponent<GameInformation>().organizerID = e.OrganizerID;

        Debug.Log("GI NAME: " + gi.name);
        if (gi.name == "gv_3" || gi.name == "gv_4")
        {
            gi.transform.Find("Play_btn").GetComponent<Button>().onClick.AddListener(() => StartWordGame(e.OrganizerID));
        }
        else if (gi.name == "gv_1" || gi.name == "gv_2")
        {
            gi.transform.Find("Play_btn").GetComponent<Button>().onClick.AddListener(() => StartMatch3Game(e.OrganizerID));
        }
    }

    void StartWordGame(int organizerID) {
        GameObject.Find("SkinManager").GetComponent<SkinManager>().ActiveSkin = (SkinManager.Reskins)organizerID-1;
        SceneManager.LoadScene("Wordscramble/Scenes/GameScene");
    }

    void StartMatch3Game(int organizerID)
    {
        GameObject.Find("SkinManager").GetComponent<SkinManager>().ActiveSkin = (SkinManager.Reskins)organizerID-1;
        SceneManager.LoadScene("Match3/Scenes/SampleScene");
    }

    private class GameModel
    {
        [JsonProperty("ID")]
        public int ID { get; set; }
        [JsonProperty("EventID")]
        public int EventID { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Words")]
        public string Words { get; set; }
    }

    private class EventModel
    {
        [JsonProperty("ID")]
        public int EventID { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("GameID")]
        public string GameID { get; set; }
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("Image")]
        public string Image { get; set; }
        [JsonProperty("OrganizerID")]
        public int OrganizerID { get; set; }
        [JsonProperty("LeaderboardID")]
        public int LeaderboardID { get; set; }
    }

    private class LeaderboardModel 
    {
        [JsonProperty("ID")]
        public int LeaderbordID { get; set; }
        [JsonProperty("Scores")]
        public List<int> Scores { get; set; }
        [JsonProperty("Players")]
        public List<string> Players { get; set; }
    }

    public void LimitScroll() { 
        
    }
}
