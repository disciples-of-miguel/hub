﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using TMPro;

public class gameViewEvents : MonoBehaviour
{

    public GameObject info_view;

    UnityEvent display_game_info;

    public bool info_view_is_active = false;

    // Start is called before the first frame update
    void Start()
    {
        //info_view.transform.gameObject.SetActive(false);

        if (display_game_info == null)
            display_game_info = new UnityEvent();

        //display_game_info.AddListener(() => { DisplayGameInfo(false); });
    }

    // Update is called once per frame
    void Update()
    {

        if(EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject == transform.gameObject){
            if (Input.anyKeyDown && display_game_info != null)
            {
                Debug.Log("TEST");
                display_game_info.AddListener(() => { DisplayGameInfo(true); });
            }
        }else if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.name == "Back_btn"){
            if (Input.anyKeyDown && display_game_info != null)
            {
                display_game_info.AddListener(() => { DisplayGameInfo(false); });
            }
        }

        display_game_info.Invoke();
    }

    void DisplayGameInfo(bool display){
        if(display){
            info_view.transform.gameObject.GetComponent<Animator>().Play("info_view_slide_in");
            info_view.transform.Find("Players_text").transform.gameObject.GetComponent<TMP_Text>().text = "Players: 10";
        }else{
            info_view.transform.gameObject.GetComponent<Animator>().Play("info_view_slide_out");
        }
    }
}
